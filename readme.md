# Juniorerb web

Webpage built on react for juniorerb competition, now open for outside contributions

## Setup

### Application
1) Execute `npm i` in root directory
2) Execute `npm i` in `pacakages/frontend`
3) Execute `npm run start_development` in `packages/frontend`

### WebStorm/PHPStorm IDE
Add czech dictionary located in `cs.dic` in project root <br/>
To do that do following: <br>
1) Open settings through `File -> Settings` or press `ctrl+alt+s`
2) In settings go to `Editor -> Spelling`
3) In top box click plus icon
4) Navigate to project root
5) Select `cs.dic` and confirm
<br> then can start the stack by pressing green play button next to |start_development| box, at [localhost:3000](http://localhost:3000) it should now run

### Visual Studio Code
1) Open projects workspace file
2) Install recommended extensions


## Requirements
Node.js + npm 

## Deployment

### Frontend
1) Make sure you ran `npm install` or `npm i` at least once
2) Run `npx next build` or `sudo npx next build` at `packages/frontend` if you have global node or `"<path to your nvm install>/npx" next build` or `sudo "<path to your nvm install>/npx" next build` at `packages/frontend` in case you use nvm
3) Run `sudo npx next start -p 80` at `packages/frontend` if you use globaly installed node or `sudo "<path to your nvm install>/npx" next start -p 80` at `packages/frontend`
4) For new content `git pull`, then repeat from step 2

## Shit we use and stuff
### Technologies
* node
* react
* ESLint + Prettier

### Resources
* coolors palette 
* Montserrat and Open Sans fonts from google fonts
* Few images borrowed from interwebz

## Pokyny pro příspěvky

* Jak přidám nový obsah?
    * Forkneš si projekt, v `development` branchi upravíš obsah stránek a potom vytvoříš merge request. Když ho potvrdíme pošleme k nám do vývojové větve, stane se tvůj obsah součástí našeho webu
* Dostanu za to něco?
    * Ne.
* Jsou nějaké vaše speciální požadavky na kvalitu?
    * Ne, ale oceníme pokud se budeš držet faktů (Každý MR je otevřen diskuzi cca 3 týdny než je poslán do hlavní větve, pokud nepůjde o něco významného)
* Jak poznám aktuální verzi?
    * Pro každou revizi budeme dávat seznam změn do sekce O Webu, pokud se bude jednat o informace na stránce O mně, bude posledních `n` revizí zobrazeno dole v hranatých závorkách
    