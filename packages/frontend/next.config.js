// eslint-disable-next-line @typescript-eslint/no-var-requires
const withLess = require("@zeit/next-less")
// eslint-disable-next-line @typescript-eslint/no-var-requires
const withCSS = require("@zeit/next-css")
module.exports = withCSS(
	withLess({
		webpack(config) {
			config.module.rules.push({
				test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
				use: {
					loader: "url-loader",
					options: {
						limit: 100000,
					},
				},
			})
			return config
		},
		cssModules: false,
		cssLoaderOptions: {
			importLoaders: 1,
			localIdentName: "[local]___[hash:base64:5]",
		},
	}),
	{
		cssModules: false,
	},
)
