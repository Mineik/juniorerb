import React from "react"
import { Layout } from "../components/layout"
import { Menu } from "../components/Menu"
import { ContentMain } from "../components/content/Main"
import { ContentAbout } from "../components/content/About"
import { ContentPhotos } from "../components/content/Gallery"
import { WebAboutContent } from "../components/content/About-web"
import "../less/style.less"
export function Index() {
	const [page, setPage] = React.useState(0)

	React.useEffect(() => {
		switch (window.location.pathname) {
			case "/":
				if (page !== 0) setPage(0)
				break
			case "/about":
			case "/about.html":
				if (page !== 1) setPage(1)
				break
			case "/galerie":
				if (page !== 2) setPage(2)
				break
			case "/about-web":
			case "/about-web.html":
				if (page !== 3) setPage(3)
				break
			default:
				if (page !== 666) setPage(666)
				break
		}
	})

	return (
		<Layout>
			<Menu page={page} />

			{(() => {
				//function load content
				console.log(`Loading content for ${page}`)
				switch (page) {
					case 1:
						console.log("setting about Content")
						return <ContentAbout />
					case 0:
						console.log("setting main content")
						return <ContentMain />
					case 3:
						console.log("setting about web Content")
						return <WebAboutContent />
					default:
						console.log("fallthrough :shrug:")
						return <ContentMain />
				}
			})()}
		</Layout>
	)
}
