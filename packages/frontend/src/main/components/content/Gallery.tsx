import React from "react"

export function ContentPhotos() {
	return (
		<div className="content" id="gallery">
			<h2>Napřed</h2>
			<p>
				Veškerý materiály na tomhle webu jsou pro vzdělávací účely.
				Většina obrázku na našem webu je linknutá přímo ze zdroje, často
				se na něj jde rovnou prokliknout.
			</p>
			<div className="fotkyContainer">
				<div className="obrazek">
					<h2>Původní úvodní obrázek</h2>
					<img src="/img/genialni-uvodak.png" width="100%" />
				</div>
				<div className="obrazek">
					<h2>Aktuální úvodní obrázek</h2>
					<img src="/img/uvodak2.png" width="100%" />
				</div>
				<div className="obrazek">
					<h2>Favicona</h2>
					<img src="/icon/favicon.png" />
				</div>
			</div>
		</div>
	)
}
