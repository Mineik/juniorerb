import React from "react"
import HorizontalTimeline from "react-horizontal-timeline"

const hodnoty = [
	"1895-7-3",
	"1916",
	"1919-1",
	"1919-1-5",
	"1922-7-28",
	"1944-9",
	"1948-5-5",
	"1949-1-28",
	"1949-6-21",
]

const highlighty = [
	{
		displayDatum: "7. Března 1895",
		nadpis: "narození",
		obsah: "Narodil jsem se u opavy ;)",
	},
	{
		displayDatum: "1916",
		nadpis: "odvedení na haličskou frontu",
		obsah: "Bojoval jsem v první světové válce",
	},
	{
		displayDatum: "Leden 1919",
		nadpis: "Návrat do Českolovenska",
		obsah: "Už jsem byl Poručík",
	},
	{
		displayDatum: "Leden 1919",
		nadpis: "Spor o těšínsko",
		obsah: "bojovali jsme o sporné území s polskem",
	},
	{
		displayDatum: "28. Července 1922",
		nadpis: "Nejšťastnější den?",
		obsah: "Narodil se mi syn!",
	},
	{
		displayDatum: "Září 1944",
		nadpis: "Tenkrát u kyjeva",
		obsah: "Jsme podepsali spojeneckou smlouvu s Rusy",
	},
	{
		displayDatum: "5. Května 1948",
		nadpis: "S$@!# Rudý",
		obsah: "Přišli si pro mně až do nemocnice!",
	},
	{
		displayDatum: "Konec Ledna 1949",
		nadpis:
			"Právě jsem se přiznal, že jsem asi zabil arcivévodu ferdinanda",
		obsah: "Odsoudili mě ke smrti oběšením, prý za vlastizradu...",
	},
	{
		displayDatum: "21. Června 1949",
		nadpis: "smrt",
		obsah: "Popravili mě komunuisté na dvorku borské věznice v Plzni",
	},
]

const pamatniky = [
	{
		obrazekLink:
			"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Heliodor.Pika.Dablicky.Hrbitov.jpg/220px-Heliodor.Pika.Dablicky.Hrbitov.jpg",
		name: "Symbolický hrob na ďáblickém hřbitově v Praze",
		builtAt: "1992",
		content: "Kamenný Náhrobek",
	},
	{
		obrazekLink:
			"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Park_Piky.jpg/220px-Park_Piky.jpg",
		name: "Parčík věnovaný Píkovi v České Lípě",
		builtAt: "1999",
		content:
			"Vybudován na popud Josefa Hejtmánka, předsedy místní pobočky Konfederace politických vězňů.",
	},
	{
		obrazekLink:
			"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Pam.deska.GSACR.Dejvice.Vlevo.jpg/220px-Pam.deska.GSACR.Dejvice.Vlevo.jpg",
		name: "Památní deska v Praze - Dejvcích na budově GŠ",
		builtAt: "1991",
		content:
			'text: "PAMÁTCE VYNIKAJÍCÍHO VOJÁKA, DIPLOMATA A VLASTENCE, DIVIZNÍHO GENERÁLA HELIODORA PÍKY NEZÁKONNĚ POPRAVENÉHO 21. 6. 1949", v roce 1998 připojena jména vojáků, kteří se stali oběťmi komunistického režimu',
	},
]

export function ContentMain() {
	const [index, setIndex] = React.useState(0)
	return (
		<div className="content" id={"main"}>
			<h1>Vítejte na webu věnovanému Heliodoru Píkovi</h1>
			<p>
				Našim cílem je přiblížit vám život Generála Heliodora Píky,
				popraveného komunistickým režimem v roce 1950
			</p>
			<h2>Kde ho najdete</h2>
			<div className="pomnikyContainer">
				{(() => {
					const s = pamatniky.map(e => {
						return (
							<div className="pomnik">
								<a
									title="Zdroj Obrázku ;)"
									href={e.obrazekLink}
									className="pomnikImage"
								>
									<img src={e.obrazekLink} />
								</a>
								<h3 className="pomnikTitle">{e.name}</h3>
								<p className="pomnikContent">
									Vybudován: {e.builtAt} <br /> {e.content}
								</p>
							</div>
						)
					})
					return s
				})()}
			</div>
			<h2>Rychlé shrnutí mého života</h2>
			<div id={"timeline"}>
				<HorizontalTimeline
					index={index}
					indexClick={index => {
						setIndex(index)
					}}
					values={hodnoty}
					styles={{
						fontSize: "8px",
						background: "#006BA6",
						foreground: "#AD343E",
						outline: "#A6B1E1",
					}}
					getLabel={date => {
						const a = hodnoty.indexOf(date)
						if (a !== -1) return highlighty[a].displayDatum
						else return "idk"
					}}
					isOpenBeginning={false}
					isOpenEnding={false}
				/>
			</div>
			<div id="item-shit" className="item-highlight">
				<h3 className="item item-highlight-title" id="item-card">
					{highlighty[index].nadpis}
				</h3>
				<p className="item">{highlighty[index].obsah}</p>
			</div>
			<span id="napoveda">
				Více se dozvíte na stránce <a href="about">O mně</a>
			</span>
		</div>
	)
}
