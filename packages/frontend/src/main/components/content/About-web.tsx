import React from "react"

export function WebAboutContent() {
	return (
		<div className="content">
			<p>
				Tenhle web má sloužit jako web, který přibližuje život Heliodora
				Píky. Nepřišlo mi vhodné, takový zdroj informací bude uzavřen
				úpravám lidem mimo mojí bublinu, tak jsem se rozhodl otevřít
				jeho zdrojový kód tak, že každý může přispět k jeho úpravám.
			</p>
			<p>
				V patičce proto najdete gitový repositář, kde můžete přispívat
				do našeho zdrojového kódu svými poznatky, příspěvky a námitkami.
			</p>
			<p>Nevíte jak přispívat? To tam najdete také!</p>
			<h2>Kontaktujte nás:</h2>
			<span className="role">Project Leader:</span> Daniel Brada
			<a href="mailto:kontakt@bradad.tk">
				{"<"}kontakt@bradad.tk{">"}
			</a><a href="https://gitlab.com/Mineik"><span className="fab fa-gitlab" /> Gitlab</a>

		</div>
	)
}
