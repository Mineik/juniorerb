import React from "react"

export function ContentAbout() {
	return (
		<div className="content" id="about">
			<h1>O mně</h1>
			Jak jsem žil?
			<h2>Život před první světovou válkou</h2>
			<p>
				&nbsp;Narodil jsem se 3. července 1897 ve Štítině u opavy, jsem
				syn vesnického koláře. Můj otec se jmenoval Ignác. Když jsem v
				roce 1915 dostudoval, chtěl jsem jít na farmacii, ale byl jsem
				odveden k zeměbraneckému regimentu. Sloužil jsem tam asi rok,
				když jsem byl odeslán na haličskou frontu, kde mě v červenci
				1916 zajali ruští vojáci. Po třech měsících jsem se přihlásil do
				československých legií, kam jsem byl zařazen 21. července 1917
				jako Vojín.
			</p>
			<h2>Já za první republiky</h2>
			<p>
				&nbsp;V lednu 1919 jsem se vrátil do tehdy už nového státu.
				Československa. Tenkrát už jsem byl Poručíkem.
			</p>
			<p>
				&nbsp;Hned co jsem se vrátil, byl jsem poslán do boje proti
				polsku do boje o Těšínsko. Ten se sice definitivně rozsekl až po
				druhé světové válce, 9 let po mojí smrti v roce 1958, ale díky
				obětavému nasazení československé armády a hlavně našemu
				generálu Josefu Šnejdárkovi, jsme získali alespoň část sporného
				území.
			</p>
			<p>
				&nbsp;Byl jsem byl převelen na slovenskou frontu, ale odtud mě
				brzy poslali na vojenskou školu ve francii. Dostudoval jsem v
				roce 1920 a stal jsem se učitelem vojenské školy v hranicích. V
				roce 1921 jsem se oženil a rok poté se nám narodil syn - Milan.
			</p>
			<p>
				&nbsp;V roce 1923 jsem jako kapitán začal sloužit v generálním
				štábu československých branných sil. V roce 1926 jsem odjel
				studovat vojenskou vysokou školu v Paříži. Do čech jsem se
				vrátil v roce 1928.
			</p>
			<p>
				&nbsp;Od roku 1932 jsem byl vojenským {'"'}atašé{'"'} (takový
				cool označení pro nižšího diplomatického pracovníka) v
				Bukurešti. Bylo to tenkrát dost důležitý. Rumunsko bylo totiž
				důležitá opora proti německu, které se roztahovalo a tlačilo na
				nás a maďarsko, které chtělo část našeho území také. Tuto funkci
				jsem zastával až do roku 1937, kdy jsem se vrátil do
				československého generálního štábu a tam mě povýšili do hodnosti
				Plukovníka generálního štábu.
			</p>
			<h2>A pak se to pos.... (2. světová válka)</h2>
			<p>
				&nbsp;V roce 1938 jsem se snažil domluvit s Jugoslávií a
				Bulharskem, aby nám když na nás německo zaútočí poskytli
				materiální výpomoc. A pak to šlo do kopru. (alespoň pro
				československo) Byli jsme donuceni přejít pod německou ochranu a
				z československa se stal Protektorát Čechy a Morava. Tenkrát
				jsem přes Francii utekl do Velké Británie. Tam jsem se nabídl
				Janu Masarykovi a Edvard Beneš mě vyslal jako vyslance do
				Bukurešti. Pomáhal jsem tady českoslovanským a maďarským
				uprchlíkům. V Rumunsku se ale k moci dostali fašisti a mě na
				krátkou dobu zatknuli. Přesunul jsem se tehdy do Istanbulu, tam
				jsem se setkal s Ludvíkem Svobodou, který mne požádal o předání
				žádosti Edvardu Benešovi. Ta se týkala navázání informační
				spolupráce se Sovětským Svazem. Beneš nabídku přijal a když jsem
				se s panem Svobodou setkal podruhé. Tam společně s Ruským
				generálem Fokinem navrhl vytvoření {'"'}Českých jednotek na
				území SSSR{'"'} a další informační spolupráci s Ruskem.
			</p>
			<p>
				&nbsp;Československá vláda toto přijala a tak jsem se v červenci
				1941 stal opět atašé. Tentokrát jsem navíc byl i Velitelem
				československé armády v Moskvě. Již tenkrát jsem pana Beneše
				upozornil, že SSSR neusiluje o svobodné československo, ale o
				komunistickou nadvládu. Od roku 1942 jsem začal budovat
				československou armádu, především z dobrovolníků a
				československých zajatců. Již tenkrát na nás pan Gottwald
				tlačil, aby jsme naší jednotku zpolitizovali, ale s panem
				Svobodou jsme jeho tlaku dokázali odolat.
			</p>
			<p>
				&nbsp;V září 1943 jsme jako 1. československá brigáda podíleli
				na osvobození Kyjeva. V listopadu jsem se účastnil slavnostního
				podpisu česko-sovětské spojenecké smlouvy, v prosinci jsem byl
				povýšen do hodnosti brigádního generála.
			</p>
			<p>
				&nbsp;V srpnu 1944 obsadili němci slovensko. Požádal jsem velení
				Sovětské armády o podporu slovenských povstalců Rudou armádou.
				Pan Stalin vydal příkaz k dodávce zbraní na slovensko a vyslal
				maršála Koněva, aby vedl Karpatskou operaci. (Fiasko, kde se
				Sověti nezvládli spojit se slovenskými povstalci) Když pak Rudí
				chtěli táhnout dále do československa, chtěl jsem, aby se
				velitelem osvobozujících jednotek stal pan Svoboda, Rudí
				odmítli.
			</p>
			<h2>Po válce se (ne)žilo ještě hůř</h2>
			<p>
				&nbsp;Ačkoliv jsme válku vyhráli, historie tím pro mnohé
				neskončila. Po válce se v roce 1948 chopili moci Komunisté a
				najednou začalo přituhovat. Hned za měsíc mě poslali na
				zdravotní dovolenou, měl jsem operaci žlučníku, do nemocnice si
				pro mě pak přišli 5. Května, hned na začátku června jsem byl
				přeložen do výslužby. Obvinili mě ze špionáže a vlastizrady. Na
				konci ledna 1949 mě pak odsoudili ke smrti oběšením. Popravili
				mě půl roku poté, 21. Června 1949 v Plzni na dvoře Borské
				věznice. Moje tělo {'"'}ztratili{'"'} a dodnes nenašli.
			</p>
			<span>
				[13.2.2020 - Zdroj{" "}
				<a href="https://cs.wikipedia.org/wiki/Heliodor_P%C3%ADka">
					wikipedie
				</a>
				, upraveno]
			</span>
		</div>
	)
}
