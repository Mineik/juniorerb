import React from "react"
import Head from "next/head"
import { Footer } from "./Footer"

export class Layout extends React.Component {
	public render() {
		return (
			<>
				<Head>
					<title>Můj život píky</title>

					<meta property="og:title" content="Můj život píky" />
					<meta property="og:type" content="website" />
					<meta property="og:url" content="http://pika.wz.cz" />
					<meta property="og:site_name" content="Můj život píky" />

					<meta
						property="og:description"
						content="Píka z první osoby!"
					/>

					<link
						rel="stylesheet"
						href="https://use.fontawesome.com/releases/v5.12.1/css/all.css"
						integrity="sha384-v8BU367qNbs/aIZIxuivaU55N5GPF89WBerHoGA4QTcbUjYiLQtKdrfXnqAcXyTv"
						crossOrigin="anonymous"
					/>
					<script
						src="https://kit.fontawesome.com/d5588c8408.js"
						crossOrigin="anonymous"
					/>
					<link
						href="https://fonts.googleapis.com/css?family=Montserrat|Open+Sans&display=swap"
						rel="stylesheet"
					/>
					<link rel="shortcut icon" href="/icon/favicon.png" />
				</Head>
				<div className="rakovina">
					<img src="/img/uvodak2.png" />
				</div>
				{this.props.children}
				<Footer gitLink="https://gitlab.com/Mineik/juniorerb" />
			</>
		)
	}
}
