import React from "react"

export function Footer(props) {
	return (
		<div className="footer">
			<div className="socialButtons">
				<a href={props.gitLink}>
					<span className="fab fa-gitlab" />
				</a>
			</div>
			<div className="descText">
				<p>Jsme Open-Source!</p>
			</div>
		</div>
	)
}
