import React from "react"

export function Menu(props) {
	const [page, setPage] = React.useState(0)
	return (
		<div className="menu">
			{(() => {
				console.log(props.page + " " + page)
				if (page != props.page) setPage(props.page)
			})()}
			<ul>
				<li
					className={page === 0 ? "menu-item selected" : "menu-item"}
					onClick={() => {
						if (page === 0) return
						setPage(0)
						window.location.href = "./"
					}}
				>
					Domů
				</li>
				<li
					className={page === 1 ? "menu-item selected" : "menu-item"}
					onClick={() => {
						if (page === 1) return
						setPage(1)
						window.location.href = "./about.html"
					}}
				>
					O mně
				</li>
				<li
					className={page === 3 ? "menu-item selected" : "menu-item"}
					onClick={() => {
						if (page === 3) return
						setPage(3)
						window.location.href = "./about-web.html"
					}}
				>
					O webu
				</li>
			</ul>
		</div>
	)
}
